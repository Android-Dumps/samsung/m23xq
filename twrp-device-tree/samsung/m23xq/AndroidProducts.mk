#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_m23xq.mk

COMMON_LUNCH_CHOICES := \
    omni_m23xq-user \
    omni_m23xq-userdebug \
    omni_m23xq-eng
